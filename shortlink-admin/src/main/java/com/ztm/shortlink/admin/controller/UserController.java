package com.ztm.shortlink.admin.controller;

import cn.hutool.core.bean.BeanUtil;
import com.ztm.shortlink.admin.common.convention.result.Result;
import com.ztm.shortlink.admin.common.convention.result.Results;
import com.ztm.shortlink.admin.dto.req.UserGetCodeReqDTO;
import com.ztm.shortlink.admin.dto.req.UserLoginReqDTO;
import com.ztm.shortlink.admin.dto.req.UserRegisterReqDTO;
import com.ztm.shortlink.admin.dto.req.UserUpdateReqDTO;
import com.ztm.shortlink.admin.dto.resp.ActualUserRespDTO;
import com.ztm.shortlink.admin.dto.resp.UserLoginRespDTO;
import com.ztm.shortlink.admin.dto.resp.UserRespDTO;
import com.ztm.shortlink.admin.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    /**
     * 查询用户名是否存在
     * @param username
     * @return
     */
    @GetMapping("/api/short-link/admin/v1/user/has-username")
    public Result<Boolean> hasUserName(@RequestParam("username") String username){
        return Results.success(userService.hasUserName(username));
    }

    /**
     * 发送短信验证码
     * @param requestParam
     * @return
     */
    @PostMapping("/api/short-link/admin/v1/user/code")
    public Result<Void> SendCode(@RequestBody UserGetCodeReqDTO requestParam){
        userService.sendCode(requestParam);
        return Results.success();
    }

    /**
     * 用户注册
     * @param requestParam
     * @return
     */
    @PostMapping("/api/short-link/admin/v1/user/register")
    public Result<Void> register(@RequestBody UserRegisterReqDTO requestParam){
        userService.register(requestParam);
        return Results.success();
    }


    /**
     * 用户登录
     * @param requestParam
     * @return
     */
    @PostMapping("/api/short-link/admin/v1/user/login")
    public Result<UserLoginRespDTO> login(@RequestBody UserLoginReqDTO requestParam){
        return Results.success(userService.login(requestParam));
    }

    /**
     * 查询脱敏用户信息
     * @return
     */
    @GetMapping("/api/short-link/admin/v1/user/info")
    public Result<UserRespDTO> getUserInfo(){
        return Results.success(userService.getUserInfo());
    }

    /**
     * 查询无脱敏用户信息
     * @return
     */
    @GetMapping("/api/short-link/admin/v1/actual/user/info")
    public Result<ActualUserRespDTO> getActualUserByUsername(){
        return Results.success(BeanUtil.toBean(userService.getUserInfo(),ActualUserRespDTO.class));
    }

    /**
     * 用户修改个人信息
     * @param requestParam
     * @return
     */
    @PutMapping("/api/short-link/admin/v1/user/update")
    public Result<Void> update(@RequestBody UserUpdateReqDTO requestParam){
        userService.update(requestParam);
        return Results.success();
    }


    /**
     * 用户退出登录
     */
    @DeleteMapping("/api/short-link/admin/v1/user/logout")
    public Result<Void> logout(){
        userService.logout();
        return Results.success();
    }
}
