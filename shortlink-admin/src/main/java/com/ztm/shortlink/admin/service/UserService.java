package com.ztm.shortlink.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztm.shortlink.admin.dao.entity.UserDO;
import com.ztm.shortlink.admin.dto.req.UserGetCodeReqDTO;
import com.ztm.shortlink.admin.dto.req.UserLoginReqDTO;
import com.ztm.shortlink.admin.dto.req.UserRegisterReqDTO;
import com.ztm.shortlink.admin.dto.req.UserUpdateReqDTO;
import com.ztm.shortlink.admin.dto.resp.UserLoginRespDTO;
import com.ztm.shortlink.admin.dto.resp.UserRespDTO;

/**
 * 用户管理接口层
 */
public interface UserService extends IService<UserDO> {
    /**
     * 根据用户名查询用户信息
     * @return
     */
    UserRespDTO getUserInfo();

    /**
     * 查询用户名是否存在
     * @param username
     * @return
     */
    Boolean hasUserName(String username);

    /**
     * 用户注册
     */
    void register(UserRegisterReqDTO requestParam);

    /**
     * 用户修改个人信息
     * @param requestParam
     */
    void update(UserUpdateReqDTO requestParam);

    /**
     * 用户登录
     * @param requestParam
     * @return
     */
    UserLoginRespDTO login(UserLoginReqDTO requestParam);


    /**
     * 退出登录
     */
    void logout();

    /**
     * 发送短信验证码
     * @param requestParam
     * @return
     */
    void sendCode(UserGetCodeReqDTO requestParam);
}