package com.ztm.shortlink.admin.common.constant;

/**
 * 短链接后管系统Redis缓存常量类
 */
public class RedisCacheConstant {
    public static final String SERVICE_PREFIX = "short-link:admin:";
    public static final String LOCK_USER_REGISTER_KEY = SERVICE_PREFIX + "register:lock_username:";
    public static final String USER_LOGGED_IN_KEY = SERVICE_PREFIX + "login:logged_in:";
    public static final String USER_LOGGED_IN_FLAG = "1";
    public static final String SMS_VERIFICATION_CODE_KEY = SERVICE_PREFIX + "login:verification_code:";
}
