package com.ztm.shortlink.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ztm.shortlink.admin.common.convention.result.Result;
import com.ztm.shortlink.admin.remote.dto.req.RecycleBinPageReqDTO;
import com.ztm.shortlink.admin.remote.dto.resp.ShortLinkPageRespDTO;

/**
 * 回收站管理接口层
 */
public interface RecycleBinService {
    /**
     * 回收站分页查询
     * @param requestParam
     * @return
     */
    Result<IPage<ShortLinkPageRespDTO>> pageRecycleBin(RecycleBinPageReqDTO requestParam);
}
