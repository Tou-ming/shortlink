package com.ztm.shortlink.admin.controller;

import com.ztm.shortlink.admin.common.convention.result.Result;
import com.ztm.shortlink.admin.common.convention.result.Results;
import com.ztm.shortlink.admin.dto.req.GroupOrderReqDTO;
import com.ztm.shortlink.admin.dto.req.GroupUpdateReqDTO;
import com.ztm.shortlink.admin.dto.resp.GroupListRespDTO;
import com.ztm.shortlink.admin.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class GroupController {

    public final GroupService groupService;

    /**
     * 新增分组
     * @param name
     * @return
     */
    @PostMapping("/api/short-link/admin/v1/group")
    public Result<Void> groupSave(@RequestParam("name") String name){
        groupService.groupSave(name);
        return Results.success();
    }

    /**
     * 删除分组
     * @param gid
     * @return
     */
    @DeleteMapping("/api/short-link/admin/v1/group")
    public Result<Void> groupDelete(@RequestParam("gid") String gid){
        groupService.groupDelete(gid);
        return Results.success();
    }

    /**
     * 修改短链接分组
     * @param requestParam
     * @return
     */
    @PutMapping("/api/short-link/admin/v1/group")
    public Result<Void> groupUpdate(@RequestBody GroupUpdateReqDTO requestParam){
        groupService.groupUpdate(requestParam);
        return Results.success();
    }

    /**
     * 查询短链接分组
     * @return
     */
    @GetMapping("/api/short-link/admin/v1/group/list")
    public Result<List<GroupListRespDTO>> groupList(){
        return Results.success(groupService.groupList());
    }

    /**
     * 短链接分组排序
     * @return
     */
    @PostMapping("/api/short-link/admin/v1/group/sort")
    public Result<Void> groupOrder(@RequestBody List<GroupOrderReqDTO> requestParam){
        groupService.groupOrder(requestParam);
        return Results.success();
    }
}
