package com.ztm.shortlink.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ztm.shortlink.admin.common.convention.result.Result;
import com.ztm.shortlink.admin.common.convention.result.Results;
import com.ztm.shortlink.admin.remote.ShortLinkRemoteService;
import com.ztm.shortlink.admin.remote.dto.req.RecycleBinPageReqDTO;
import com.ztm.shortlink.admin.remote.dto.req.RecycleBinRemoveReqDTO;
import com.ztm.shortlink.admin.remote.dto.req.RecycleBinSaveReqDTO;
import com.ztm.shortlink.admin.remote.dto.resp.ShortLinkPageRespDTO;
import com.ztm.shortlink.admin.service.RecycleBinService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 回收站控制层
 */
@RestController
@RequiredArgsConstructor
public class RecycleBinController {

    ShortLinkRemoteService shortLinkRemoteService = new ShortLinkRemoteService(){};

    private final RecycleBinService recycleBinService;

    /**
     * 移动至回收站
     * @param requestParam
     * @return
     */
    @PostMapping("/api/short-link/admin/v1/recycle-bin/save")
    public Result<Void> saveRecycleBin(@RequestBody RecycleBinSaveReqDTO requestParam){
        shortLinkRemoteService.saveRecycleBin(requestParam);
        return Results.success();
    }

    /**
     * 从回收站恢复
     * @param requestParam
     * @return
     */
    @PostMapping("/api/short-link/admin/v1/recycle-bin/recover")
    public Result<Void> recoverRecycleBin(@RequestBody RecycleBinSaveReqDTO requestParam){
        shortLinkRemoteService.recoverRecycleBin(requestParam);
        return Results.success();
    }

    /**
     * 从回收站删除
     * @param requestParam
     * @return
     */
    @PostMapping("/api/short-link/admin/v1/recycle-bin/remove")
    public Result<Void> removeRecycleBin(@RequestBody RecycleBinRemoveReqDTO requestParam){
        shortLinkRemoteService.removeRecycleBin(requestParam);
        return Results.success();
    }

    /**
     * 分页查询回收站
     * @param requestParam
     * @return
     */
    @GetMapping("/api/short-link/admin/v1/recycle-bin/page")
    public Result<IPage<ShortLinkPageRespDTO>> pageRecycleBin(RecycleBinPageReqDTO requestParam){
        return recycleBinService.pageRecycleBin(requestParam);
    }
}
