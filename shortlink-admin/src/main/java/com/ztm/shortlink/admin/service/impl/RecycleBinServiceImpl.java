package com.ztm.shortlink.admin.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ztm.shortlink.admin.common.context.BaseContext;
import com.ztm.shortlink.admin.common.convention.exception.ServiceException;
import com.ztm.shortlink.admin.common.convention.result.Result;
import com.ztm.shortlink.admin.dao.entity.GroupDO;
import com.ztm.shortlink.admin.dao.mapper.GroupMapper;
import com.ztm.shortlink.admin.remote.ShortLinkRemoteService;
import com.ztm.shortlink.admin.remote.dto.req.RecycleBinPageReqDTO;
import com.ztm.shortlink.admin.remote.dto.resp.ShortLinkPageRespDTO;
import com.ztm.shortlink.admin.service.RecycleBinService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 回收站管理接口实现层
 */
@Service
@RequiredArgsConstructor
public class RecycleBinServiceImpl implements RecycleBinService {

    private final GroupMapper groupMapper;

    ShortLinkRemoteService shortLinkRemoteService = new ShortLinkRemoteService(){};

    @Override
    public Result<IPage<ShortLinkPageRespDTO>> pageRecycleBin(RecycleBinPageReqDTO requestParam) {
        //根据用户名查询所有分组得到gidList
        LambdaQueryWrapper<GroupDO> queryWrapper = Wrappers.lambdaQuery(GroupDO.class)
                .eq(GroupDO::getUsername, BaseContext.getCurrentUsername())
                .eq(GroupDO::getDelFlag, 0);
        List<GroupDO> groupDOS = groupMapper.selectList(queryWrapper);
        if(CollUtil.isEmpty(groupDOS)){
            throw new ServiceException("用户无分组信息");
        }
        requestParam.setGidList(groupDOS.stream().map(GroupDO::getGid).toList());
        //通过gidList发起远程调用
        return shortLinkRemoteService.pageRecycleBin(requestParam);
    }
}
