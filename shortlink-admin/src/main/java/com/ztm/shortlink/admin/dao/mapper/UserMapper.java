package com.ztm.shortlink.admin.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ztm.shortlink.admin.dao.entity.UserDO;

public interface UserMapper extends BaseMapper<UserDO> {
}
