package com.ztm.shortlink.admin.dto.req;

import lombok.Data;

/**
 * 用户获取短信验证码请求参数
 */

@Data
public class UserGetCodeReqDTO {
    /**
     * 手机号
     */
    private String phone;
}
