package com.ztm.shortlink.admin.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ztm.shortlink.admin.dao.entity.GroupDO;

public interface GroupMapper extends BaseMapper<GroupDO> {
}
