package com.ztm.shortlink.admin.dto.req;

import lombok.Data;

/**
 * 用户登录请求参数
 */
@Data

public class UserLoginReqDTO {

    /**
     * 登录用户名
     */
    private String username;

    /**
     * 登录密码
     */
    private String password;

}
