package com.ztm.shortlink.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ztm.shortlink.admin.dao.entity.GroupDO;
import com.ztm.shortlink.admin.dto.req.GroupOrderReqDTO;
import com.ztm.shortlink.admin.dto.req.GroupUpdateReqDTO;
import com.ztm.shortlink.admin.dto.resp.GroupListRespDTO;

import java.util.List;

/**
 * 短链接管理接口层
 */
public interface GroupService extends IService<GroupDO> {

    /**
     * 新增分组
     * @param name
     */
    void groupSave(String name);

    /**
     * 新增分组
     * @param username
     * @param name
     */
    void groupSave(String username,String name);

    /**
     * 删除分组
     */
    void groupDelete(String gid);

    /**
     * 修改分组
     * @param requestParam
     */
    void groupUpdate(GroupUpdateReqDTO requestParam);

    /**
     * 查询短链接分组
     */
    List<GroupListRespDTO> groupList();

    /**
     * 短链接分组排序
     * @param requestParam
     */
    void groupOrder(List<GroupOrderReqDTO> requestParam);
}