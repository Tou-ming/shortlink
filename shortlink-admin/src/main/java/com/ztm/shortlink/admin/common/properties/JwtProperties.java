package com.ztm.shortlink.admin.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "short-link.jwt")
@Data
public class JwtProperties {

    /**
     * 用户登录与身份验证jwt令牌相关配置
     */
    private String secretKey;

    private long ttl;

    private String tokenName;
}
