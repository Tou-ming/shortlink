package com.ztm.shortlink.admin.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
@Slf4j
@RequiredArgsConstructor
public class WebMvcConfiguration extends WebMvcConfigurationSupport {

    private final JwtInterceptor jwtInterceptor;

    /**
     * 注册自定义拦截器
     *
     * @param registry
     */
    protected void addInterceptors(InterceptorRegistry registry) {
        log.info("开始注册自定义拦截器...");
        registry.addInterceptor(jwtInterceptor)
                .addPathPatterns("/api/short-link/admin/**")
                .excludePathPatterns("/api/short-link/admin/v1/user/has-username")
                .excludePathPatterns("/api/short-link/admin/v1/user/code")
                .excludePathPatterns("/api/short-link/admin/v1/user/register")
                .excludePathPatterns("/api/short-link/admin/v1/user/login");
    }
}
