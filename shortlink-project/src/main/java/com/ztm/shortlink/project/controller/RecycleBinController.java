package com.ztm.shortlink.project.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ztm.shortlink.project.common.convention.result.Result;
import com.ztm.shortlink.project.common.convention.result.Results;
import com.ztm.shortlink.project.dto.req.RecycleBinPageReqDTO;
import com.ztm.shortlink.project.dto.req.RecycleBinRemoveReqDTO;
import com.ztm.shortlink.project.dto.req.RecycleBinSaveReqDTO;
import com.ztm.shortlink.project.dto.resp.ShortLinkPageRespDTO;
import com.ztm.shortlink.project.service.RecycleBinService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * 回收站控制层
 */
@RestController
@RequiredArgsConstructor
public class RecycleBinController {

    private final RecycleBinService recycleBinService;

    /**
     * 移动至回收站
     * @param requestParam
     * @return
     */
    @PostMapping("/api/short-link/project/v1/recycle-bin/save")
    public Result<Void> saveRecycleBin(@RequestBody RecycleBinSaveReqDTO requestParam){
        recycleBinService.saveRecycleBin(requestParam);
        return Results.success();
    }

    /**
     * 从回收站恢复
     * @param requestParam
     * @return
     */
    @PostMapping("/api/short-link/project/v1/recycle-bin/recover")
    public Result<Void> recoverRecycleBin(@RequestBody RecycleBinSaveReqDTO requestParam){
        recycleBinService.recoverRecycleBin(requestParam);
        return Results.success();
    }

    /**
     * 从回收站删除
     * @param requestParam
     * @return
     */
    @PostMapping("/api/short-link/project/v1/recycle-bin/remove")
    public Result<Void> removeRecycleBin(@RequestBody RecycleBinRemoveReqDTO requestParam){
        recycleBinService.removeRecycleBin(requestParam);
        return Results.success();
    }

    /**
     * 分页查询回收站
     * @param requestParam
     * @return
     */
    @GetMapping("/api/short-link/project/v1/recycle-bin/page")
    public Result<IPage<ShortLinkPageRespDTO>> pageRecycleBin(RecycleBinPageReqDTO requestParam){
        return Results.success(recycleBinService.pageRecycleBin(requestParam));
    }
}
