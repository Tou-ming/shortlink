package com.ztm.shortlink.project.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ztm.shortlink.project.dao.entity.ShortLinkDO;
import com.ztm.shortlink.project.dao.mapper.ShortLinkMapper;
import com.ztm.shortlink.project.dto.req.RecycleBinPageReqDTO;
import com.ztm.shortlink.project.dto.req.RecycleBinRemoveReqDTO;
import com.ztm.shortlink.project.dto.req.RecycleBinSaveReqDTO;
import com.ztm.shortlink.project.dto.resp.ShortLinkPageRespDTO;
import com.ztm.shortlink.project.service.RecycleBinService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import static com.ztm.shortlink.project.common.constant.RedisKeyConstant.GOTO_IS_NULL_KEY;
import static com.ztm.shortlink.project.common.constant.RedisKeyConstant.GOTO_ORIGIN_URL_KEY;

/**
 * 回收站接口实现层
 */
@Service
@RequiredArgsConstructor
public class RecycleBinServiceImpl extends ServiceImpl<ShortLinkMapper, ShortLinkDO> implements RecycleBinService {

    private final StringRedisTemplate stringRedisTemplate;

    /**
     * 移动至回收站
     * @param requestParam
     */
    @Override
    public void saveRecycleBin(RecycleBinSaveReqDTO requestParam) {
        //根据gid和fullShortUrl修改enable_status字段为1

        LambdaQueryWrapper<ShortLinkDO> queryWrapper = Wrappers.lambdaQuery(ShortLinkDO.class)
                .eq(ShortLinkDO::getFullShortUrl, requestParam.getFullShortUrl())
                .eq(ShortLinkDO::getGid, requestParam.getGid())
                .eq(ShortLinkDO::getEnableStatus, 0)
                .eq(ShortLinkDO::getDelFlag, 0);
        ShortLinkDO shortLinkDO = ShortLinkDO.builder()
                .enableStatus(1)
                .build();
        baseMapper.update(shortLinkDO,queryWrapper);

        //移至回收站后，需要把跳转链接的缓存删掉
        stringRedisTemplate.delete(GOTO_ORIGIN_URL_KEY + requestParam.getFullShortUrl());
    }

    /**
     * 从回收站恢复
     * @param requestParam
     */
    @Override
    public void recoverRecycleBin(RecycleBinSaveReqDTO requestParam) {
        //根据gid和fullShortUrl修改enable_status字段为0

        LambdaQueryWrapper<ShortLinkDO> queryWrapper = Wrappers.lambdaQuery(ShortLinkDO.class)
                .eq(ShortLinkDO::getFullShortUrl, requestParam.getFullShortUrl())
                .eq(ShortLinkDO::getGid, requestParam.getGid())
                .eq(ShortLinkDO::getEnableStatus, 1)
                .eq(ShortLinkDO::getDelFlag, 0);
        ShortLinkDO shortLinkDO = ShortLinkDO.builder()
                .enableStatus(0)
                .build();
        baseMapper.update(shortLinkDO,queryWrapper);

        //从回收站恢复后，需要把缓存的空值删掉
        stringRedisTemplate.delete(GOTO_IS_NULL_KEY + requestParam.getFullShortUrl());
    }

    /**
     * 从回收站删除
     * @param requestParam
     */
    @Override
    public void removeRecycleBin(RecycleBinRemoveReqDTO requestParam) {
        //根据gid和fullShortUrl修改del_flag字段为1

        LambdaQueryWrapper<ShortLinkDO> queryWrapper = Wrappers.lambdaQuery(ShortLinkDO.class)
                .eq(ShortLinkDO::getFullShortUrl, requestParam.getFullShortUrl())
                .eq(ShortLinkDO::getGid, requestParam.getGid())
                .eq(ShortLinkDO::getEnableStatus, 1)
                .eq(ShortLinkDO::getDelFlag, 0);

        ShortLinkDO shortLinkDO = new ShortLinkDO();
        shortLinkDO.setDelFlag(1);

        baseMapper.update(shortLinkDO,queryWrapper);
    }

    /**
     * 分页查询回收站
     * @param requestParam
     * @return
     */
    @Override
    public IPage<ShortLinkPageRespDTO> pageRecycleBin(RecycleBinPageReqDTO requestParam) {
        LambdaQueryWrapper<ShortLinkDO> queryWrapper = Wrappers.lambdaQuery(ShortLinkDO.class)
                .in(ShortLinkDO::getGid, requestParam.getGidList())
                .eq(ShortLinkDO::getEnableStatus, 1)
                .eq(ShortLinkDO::getDelFlag, 0)
                .orderByDesc(ShortLinkDO::getUpdateTime);
        IPage<ShortLinkDO> resultPage = baseMapper.selectPage(requestParam, queryWrapper);
        return resultPage.convert(each -> BeanUtil.toBean(each, ShortLinkPageRespDTO.class));
    }
}
