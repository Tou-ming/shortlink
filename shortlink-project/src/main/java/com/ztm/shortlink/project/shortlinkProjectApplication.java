package com.ztm.shortlink.project;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.ztm.shortlink.project.dao.mapper")
public class shortlinkProjectApplication {
    public static void main(String[] args) {
        SpringApplication.run(shortlinkProjectApplication.class);
    }
}
