package com.ztm.shortlink.project.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ztm.shortlink.project.dao.entity.ShortLinkDO;
import com.ztm.shortlink.project.dto.req.RecycleBinPageReqDTO;
import com.ztm.shortlink.project.dto.req.RecycleBinRemoveReqDTO;
import com.ztm.shortlink.project.dto.req.RecycleBinSaveReqDTO;
import com.ztm.shortlink.project.dto.resp.ShortLinkPageRespDTO;

/**
 * 回收站接口层
 */
public interface RecycleBinService extends IService<ShortLinkDO> {
    /**
     * 移动至回收站
     * @param requestParam
     */
    void saveRecycleBin(RecycleBinSaveReqDTO requestParam);

    /**
     * 从回收站恢复
     * @param requestParam
     */
    void recoverRecycleBin(RecycleBinSaveReqDTO requestParam);

    /**
     * 从回收站删除
     * @param requestParam
     */
    void removeRecycleBin(RecycleBinRemoveReqDTO requestParam);

    /**
     * 分页查询回收站
     * @param requestParam
     * @return
     */
    IPage<ShortLinkPageRespDTO> pageRecycleBin(RecycleBinPageReqDTO requestParam);
}
