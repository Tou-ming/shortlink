package com.ztm.shortlink.project.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ztm.shortlink.project.common.convention.result.Result;
import com.ztm.shortlink.project.common.convention.result.Results;
import com.ztm.shortlink.project.dto.req.ShortLinkCreateReqDTO;
import com.ztm.shortlink.project.dto.req.ShortLinkPageReqDTO;
import com.ztm.shortlink.project.dto.req.ShortLinkUpdateReqDTO;
import com.ztm.shortlink.project.dto.resp.ShortLinkCountQueryRespDTO;
import com.ztm.shortlink.project.dto.resp.ShortLinkCreateRespDTO;
import com.ztm.shortlink.project.dto.resp.ShortLinkPageRespDTO;
import com.ztm.shortlink.project.service.ShortLinkService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 短链接控制层
 */
@RestController
@RequiredArgsConstructor
public class ShortLinkController {

    private final ShortLinkService shortLinkService;

    /**
     * 创建短链接
     * @return
     */
    @PostMapping("/api/short-link/project/v1/create")
    public Result<ShortLinkCreateRespDTO> createShortLink(@RequestBody ShortLinkCreateReqDTO requestParam){
        return Results.success(shortLinkService.createShortLink(requestParam));
    }


    /**
     * 修改短链接
     * @param requestParam
     * @return
     */
    @PostMapping("/api/short-link/project/v1/update")
    public Result<Void> updateShortLink(@RequestBody ShortLinkUpdateReqDTO requestParam){
        shortLinkService.updateShortLink(requestParam);
        return Results.success();
    }

    /**
     * 分页查询短链接
     * @param requestParam
     * @return
     */
    @GetMapping("/api/short-link/project/v1/page")
    public Result<IPage<ShortLinkPageRespDTO>> pageShortLink(ShortLinkPageReqDTO requestParam){
        return Results.success(shortLinkService.pageShortLink(requestParam));
    }

    /**
     * 查询短链接分组内数量
     */
    @GetMapping("/api/short-link/project/v1/count")
    public Result<List<ShortLinkCountQueryRespDTO>> listGroupShortLinkCount(@RequestParam("gidList") List<String> requestParam){
        return Results.success(shortLinkService.listGroupShortLinkCount(requestParam));
    }

    /**
     * 短链接跳转
     * @param shorUri
     * @param request
     * @param response
     */
    @GetMapping("/{short-uri}")
    public void restoreUrl(
            @PathVariable("short-uri") String shorUri,
            HttpServletRequest request,
            HttpServletResponse response){
        shortLinkService.restoreUrl(shorUri,request,response);
    }

}
