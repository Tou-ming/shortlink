package com.ztm.shortlink.project.utils;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;

import java.util.Date;
import java.util.Optional;

import static com.ztm.shortlink.project.common.constant.ShortLinkConstant.DEFAULT_CACHE_VALID_TIME;


public class LinkUtil {
    /**
     * 获取短链接缓存有效期
     * @param validDate
     * @return
     */
    public static long getLinkCacheValidTime(Date validDate){
        //如果有效期为空，表明为永久类型的短链接，则短链接缓存有效时间为默认值(1月)
        //如果有效期有值，根据有效期-当前值计算缓存有校时间，转为单位ms
        return Optional.ofNullable(validDate)
                .map(each-> DateUtil.between(new Date(),each,DateUnit.MS))
                .orElse(DEFAULT_CACHE_VALID_TIME);
    }
}
