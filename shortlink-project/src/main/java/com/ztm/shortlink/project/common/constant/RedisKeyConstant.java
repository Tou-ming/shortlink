package com.ztm.shortlink.project.common.constant;

/**
 * Redis Key 常量类
 */
public class RedisKeyConstant {
    /**
     * 业务前缀
     */
    public static final String SERVICE_PREFIX = "short-link:project:";
    /**
     * 短链接跳转Key前缀
     */
    public static final String GOTO_ORIGIN_URL_KEY = SERVICE_PREFIX + "goto:";

    /**
     * 短链接空值跳转前缀
     */
    public static final String GOTO_IS_NULL_KEY = SERVICE_PREFIX + "goto_is_null:";

    /**
     * 短链接跳转分布式锁前缀
     */
    public static final String LOCK_GOTO_ORIGIN_URL_KEY = SERVICE_PREFIX + "lock_goto:";
}
