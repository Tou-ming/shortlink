package com.ztm.shortlink.project.common.constant;

/**
 * 短链接常量类
 */
public class ShortLinkConstant {
    /**
     * 短链接默认缓存时间1月=2626560000ms
     */
    public static final long DEFAULT_CACHE_VALID_TIME = 2626560000L;
}
