package com.ztm.shortlink.project.dto.req;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ztm.shortlink.project.dao.entity.ShortLinkDO;
import lombok.Data;

import java.util.List;

/**
 * 回收站分页查询请求参数
 */
@Data
public class RecycleBinPageReqDTO extends Page<ShortLinkDO> {
    /**
     * 该用户分组标识列表
     */
    private List<String> gidList;
}
