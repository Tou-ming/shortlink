package com.ztm.shortlink.project.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ztm.shortlink.project.dao.entity.ShortLinkDO;
import com.ztm.shortlink.project.dto.req.ShortLinkCreateReqDTO;
import com.ztm.shortlink.project.dto.req.ShortLinkPageReqDTO;
import com.ztm.shortlink.project.dto.req.ShortLinkUpdateReqDTO;
import com.ztm.shortlink.project.dto.resp.ShortLinkCountQueryRespDTO;
import com.ztm.shortlink.project.dto.resp.ShortLinkCreateRespDTO;
import com.ztm.shortlink.project.dto.resp.ShortLinkPageRespDTO;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.util.List;

/**
 * 短链接接口层
 */
public interface ShortLinkService extends IService<ShortLinkDO>  {

    /**
     * 创建短链接
     * @param requestParam
     * @return
     */
    ShortLinkCreateRespDTO createShortLink(ShortLinkCreateReqDTO requestParam);

    /**
     * 分页查询短链接
     * @param requestParam
     * @return
     */
    IPage<ShortLinkPageRespDTO> pageShortLink(ShortLinkPageReqDTO requestParam);

    /**
     * 短链接分组内数量
     * @param requestParam
     * @return
     */
    List<ShortLinkCountQueryRespDTO> listGroupShortLinkCount(List<String> requestParam);

    /**
     * 修改短链接
     * @param requestParam
     */
    void updateShortLink(ShortLinkUpdateReqDTO requestParam);

    /**
     * 短链接跳转
     * @param shorUri
     * @param request
     * @param response
     */
    void restoreUrl(String shorUri, HttpServletRequest request, HttpServletResponse response);
}
